
// Express.JS - Mongoose ODM

const express = require("express");

// Mongoose is a package that allows creation of Schemas to model ou data structures.
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// MongoDB connection
	// connect the database by passing in our connection string, remeber to replace the password and database names with actual values

		// Syntax

			// mongoose.connect("<MongoDB connection string>", {useNewUrlParser: true})

			// {useNewUrlParser:true} allows us to avoid any current and future errors while connecting to MongoDB


// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://admin123:admin123@project0.8uhvgjn.mongodb.net/s35?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);


// Set notifications for connection success or failure

// Connection to the database
	// allows us to hanlde errors when the initial connection is established
	// works with the on and once Mongoose methods

let db = mongoose.connection;

db.on("error", console.error.bind(console,"connection error"));
// If a connection errorr occured, output in the console
// console.error.bind(console) allows us to print errors in our terminal

db.once("open", () => console.log("We're connected to the cloud database"));


// Mongoose Schemas
	// Schemas determine the structures of the documents to be written in the database
// Schema act as blueprints to our data
// use the Schema() constructor of the Mongoose module to create a new Schema
// The "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}

})


// Models
	// uses Schemas and are used to create/instantiate objects that correspond to the Schema
	// Server>Schema(blueprint)>Database>Collection
	// Model naming convention should be singular and capital first letter

const Task = mongoose.model("Task", taskSchema);


// Create a new task

// Business Logic
	/*
		1. Add a functionality to check of there are duplicate tasks
			- if the task already exists in the database, we return an error
			- if the task does not exists in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task objects with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
	*/

app.post("/tasks", (req,res) => {

	Task.findOne({name: req.body.name}, (err,result) => {
		if (result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New task created");
				}
			})
		}
	})

})


// Getting all the tasks

// Business Logic
	/*
		1. Retrieve all the documents
		2. If an error is encountered, print the error
		3. If no errors are found, send a success status back to the client/Postman and return an array of documents
	*/

app.get("/tasks", (req,res) => {
	Task.find({}, (err,result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})


// Activity
	
// Create a user Schema
const userSchema = new mongoose.Schema({

	username: String,
	password: String
})


// Create a user model
const User = mongoose.model("User", userSchema);

// POST Route that will access the /signUp route
app.post("/signUp", (req, res) => {

	User.findOne({username: req.body.username}, (err,result) => {
		if (result != null && result.username == req.body.username){
			return res.send("User already exists");
		} else {
			if(req.body.username !== "" && req.body.password !== "") {

				let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user registered");
				}
			})

			} else {
				return res.send("Please Enter a username and password!");
			}			
		}
	})
})

app.get("/signUp", (req,res) => {
	User.find({}, (err,result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})


app.listen(port,() => console.log(`Server running at port ${port}`));
